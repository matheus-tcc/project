# API Manager
## Backend

This project is being developed by [Matheus Heiden](https://gitlab.com/matheusheiden) as his *bachelor`s degree thesis*.

### What is this?

This repository contains the *backend* features of the API Manager and a API to handle requests from the frontend application.


### Features 

* Supports SOAP and RESTFul protocol
* Mitigates request overflow
* ...




