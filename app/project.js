'use strict'
const App = require.main.require('./app/code/core/model/app.js')

class Project {
    
    constructor (executionFlags)  {
        this.executionFlags = executionFlags
        this.app = new App();
        global.Project = this
    }

    run () {
        this.getApp().log("Starting application")
        this.app.start()
    }

    getApp() {
        return this.app
    }

    log(msg, level="info") {
        this.getApp().log(msg, level)
    }

    dispatchEvent (eventName, data) {
        this.getApp().dispatchEvent(eventName, data)
    }

    getModel(modelName, ...params) {
        try {
            return this.getApp().getModel(modelName, params)
        }
        catch (err) {
            this.log(err, 'error')
            return null
        }
    }

    getSingleton(modelName) {
        try {
            return this.getApp().getSingleton(modelName)
        }
        catch (err) {
            this.log(err, 'error')
            return null
        }
    }

    getClass(modelName) {
        try {
            return this.getApp().getClass(modelName)
        }
        catch (err) {
            this.log(err, 'error')
            return null
        }
    }

    async getConfig(path, scope ='global') {
        let config = this.getApp().getModel('core/config_data')
        await config.load(path)

        if (config.isLoaded()) {
            return config._data.value
        }

        return null
    }
}

module.exports = Project