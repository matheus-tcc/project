
const Handler = Project.getClass('api_manager/handler')

const o2x = require('object-to-xml');

const http = require('request-promise')

class Request extends Handler {
    /**
     * executes before every request
     */
    async _preDispatch() {
        this.ignoreActions = ['wsdl']
        await super._preDispatch()
        return this
    }
    /**
     * executes after every request
     */
    _postDispatch() {
        super._postDispatch()
    }
    /**
     * Executes request
     */
    async _execute() {
        if (this.request.method == 'GET') {
            await this._renderGet()
        }
        else if (this.request.method ==  'POST'){
            await this._sendPost()
        }
        else {
            throw new Error('Invalid request method')
        }

        return this
    }

    async _sendPost() {
        var self = this
        let result  = await http.post({
            url: this.application.apiUrl,
            body : this.request.rawPost
        });
        
        self.response.endStream(result)
    }

    async _renderGet() {
        if (this.request.params.hasOwnProperty('wsdl')) {
            this.response.endStream(await this._loadWsdl())
        } 
        else {
            this.response.endStream(await this.getRequest())
        }
        
    }

    async _loadWsdl () {
        if (this.application._data.wsdl) {
            return this.application._data.wsdl
        }
        let body = await this.getRequest()
        if (!body) {
            throw new Error('Could not connect to API')
        }
        let wsdl = Project.getModel('soap_api/request_wsdl', body)

        wsdl.overwriteLocation(this.application)
            
        this.application._data.wsdl = wsdl.toString()
        
        await this.application.save()

        return this.application._data.wsdl
    }

    async getRequest () {
        var self = this
        let result = ''
        try {
             result = await http.get({
                url:self.application.apiUrl, 
                qs:self.request.params
            });
        }

        catch (err) {
            if (err.statusCode == 500) {
                return err.response.body
            }
        }

        return result
    }

    async _analyze() {
        let soapRequest = {}
        if (this.request.method ==  'POST'){
            soapRequest = Project.getModel('soap_api/request_soap', this.request.rawPost)
        }
        let _analyzer = Project.getModel('soap_api/request_analyzer', soapRequest, this)

        try {
            let result = await _analyzer.analyze()
            this._id = result.request_type
        }
        catch (err) {
            console.log(err)
        }

        return this
    }

    _denyExec(message) {
        super._denyExec(message)

        this.response.endStream(
            o2x(
                { 'SOAP-ENV:Envelope' :
                    { 
                        'SOAP-ENV:Body': 
                        { 
                            'SOAP-ENV:Fault' : {
                                'faultcode' : 'sender',
                                'faultsting' : message,
                            }
                        }
                    }       
                })
        )
        return this
    }

    get request_body() {
        return this.request.rawPost
    }
}

module.exports = Request