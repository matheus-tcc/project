
const cheerio = require('cheerio')

class Xml {
    constructor(xml) {
        xml = xml.pop()
        this._oldContent = xml
        this._content = xml
        this._parsedXml = cheerio.load(xml, {
            xmlMode: true
        })
    }

    toString() {
        return this._parsedXml.xml()
    }
}

module.exports = Xml