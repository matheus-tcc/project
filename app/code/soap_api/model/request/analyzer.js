class Analyzer {
    constructor (soapRequest) {
        this.soapRequest = soapRequest[0]
        this.request = soapRequest[1]
    }

    async analyze() {
        if (this.request.request.method ==  'POST'){
            return {
                'request_type' : this.soapRequest.request_type
            }
        }
        else {
            return {
                'request_type' : 'wsdl'
            }
        }

    }

    get application () {
        return this.request.application
    }
}

module.exports = Analyzer