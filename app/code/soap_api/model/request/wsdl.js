const Xml = Project.getClass('soap_api/request_xml')

const DEFAULT_LOCATION_NODE = 'soap\\:address'

class Wsdl extends Xml {

    _replaceAddress(newAddress) {
        this._parsedXml(DEFAULT_LOCATION_NODE).attr('location', newAddress)
        return this
    }

    /**
     * 
     * @param Application application 
     */
    overwriteLocation(application) {
        this._replaceAddress(application.manager_url)
        return this
    }
}

module.exports = Wsdl