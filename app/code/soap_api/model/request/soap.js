const Xml = Project.getClass('soap_api/request_xml')

const DEFAULT_BODY_NODE = 'soapenv\\:Body'

class Soap extends Xml {
    get request_type () {
        let body = this._parsedXml(DEFAULT_BODY_NODE).children()[0]
        return this._parseName(body)
    }

    _parseName(body) {
        let name = body.name
        return name.substr(name.indexOf(':') + 1)
    }

    get request_content () {
        let body = this._parsedXml(DEFAULT_BODY_NODE).children()[0]
        return body
    }
}

module.exports = Soap

