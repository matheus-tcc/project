'use strict'

const DEFAULT_PORT = 80;

const http = require('http')

const url = require('url');

var querystring = require('querystring');

const DEFAULT_SCOPES = [
    'global',
    Project.scope
]

class Server {

    /**
     * 
     * @param {*} port 
     */
    constructor(port) {
        this.port = port ? port.pop() : DEFAULT_PORT //if port is not defined use default
        this._httpInstance = null
        

        this._router = Project.getModel('web/router')

        this.initiateRoutes()
    }


    startServer() {
        var self = this;
        this._httpInstance = http.createServer((req, res) => {
            let queryData = ''
            req.on('end', function() {
                if (queryData)  {
                    req.rawPost = queryData
                    try {
                        req.post = JSON.parse(queryData)
                    }
                    catch (err) {
                        req.post = querystring.parse(queryData)
                    }
                }
                self.handleRequest(req, res);
            })
            req.on('data', function(data) {
                if(req.method == 'POST' || req.method == 'PUT') {
                    if (typeof data == 'object') {
                        data = data.toString('utf8')
                    }
                    queryData += data;
                    
                    if(queryData.length > 1e6) {
                        queryData = "";
                        response.writeHead(413, {'Content-Type': 'text/plain'}).end();
                        req.connection.destroy();
                    }
                }   
            });
        });

        this._httpInstance.on('clientError', (err, socket) => {
            socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
        });

        this._httpInstance.listen(this.port);
    }

    handleRequest(req, res) {
        let req1 = req
        req = this._wrapRequest(req)
        res = this._wrapResponse(res)
        var queryObject = url.parse(req1.url,true).query;
        req.query = JSON.parse(JSON.stringify(queryObject))
        return this._router.route(req, res);
    }

    _wrapRequest(requestData) {
        let request = Project.getModel('web/request')
        request.wrap(requestData)
        return request
    }

    _wrapResponse(response) {
        let res = Project.getModel('web/response')
        res.wrap(response)
        return res
    }


    initiateRoutes () {
        var data = Project.getApp().module_data
        var self = this;
        let routes = []

        DEFAULT_SCOPES.forEach((scope) => {
            if (data.hasOwnProperty(scope) && data[scope].hasOwnProperty('web')) {
                for (let route of data[scope]['web'] ) {
                    console.log(route)
                    routes.push(route)
                }
            }
        })

        routes.forEach(function(route) {
            self._router.addRoute(route)
        }) 
        
    }
}

module.exports = Server