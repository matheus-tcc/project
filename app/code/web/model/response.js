'use strict'

class Response {
    constructor(){
        
    }
    wrap(response) {
        this.response = response
        return this
    }

    endStream(content) {
        if (!content)
            content = '' 

        if (typeof content  === 'object') {
            if (typeof content.toJson === 'function') {
                content = content.toJson()
            }
            else {
                content = JSON.stringify(content)
            }
        }
        this.response.header = {
            "key" : "Content-Length",
            "value" : content.length
        }
        this.response.end(content)
    }

    set status(def=200) {
        this.response.statusCode = def
        return this
    }

    set header (header) {
        this.response.setHeader(header.key, header.value)
    }
    
}

module.exports = Response