'use strict'

class Action {

    constructor () {

    }

    set router(router) {
        this._router = router;
    }

    async dispatchAction(action) {
        Project.log('Dispatching action: '+ action+ ' method: '+ this.request.method)
        this.preDispatch();

        await this[action]()

        Project.log('Post Dispatch')

        this.postDispatch();
        

    }

    get request() {
        return this._router.request
    }

    get response() {
        return this._router.response
    }

    preDispatch()  {
        /**
         * setting default response headers
         */
        this.response.header = {
            key : 'Access-Control-Allow-Origin',
            value : '*'
        }
        this.response.header = {
            key : 'Access-Control-Allow-Methods',
            value : 'POST, GET, OPTIONS, PUT'
        }
        this.response.header = {
            key : 'Access-Control-Allow-Headers',
            value : 'X-Api-Manager-Session'
        }
    }

    postDispatch() {

    }

}

module.exports = Action