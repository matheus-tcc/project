'use strict'

class Request {
    
    constructor() {
        this.request = null
    }

    wrap(request) {
        this.request = request
        return this
    }

    getPath() {
        return this.request.url;
    }

    get params() {
        return this.request.query
    }

    get query() {
        return this.request.query
    }
    set query(query) {
        this.request.query = query
    }
    getParam (param, defaultValue=false) {
        if (!this.request.query.hasOwnProperty(param)) {
            return defaultValue
        }
        return this.query[param]
    }

    get method () {
        return this.request.client.parser.incoming.method
    }

    get rawPost () {
        return this.request.rawPost
    }

    get headers () {
        return this.request.headers
    }

    get path () {
        return this.request.url
    }

    get post() {
        return this.request.post
    }
}

module.exports = Request