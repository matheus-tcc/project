'use strict'

const path = require('path')

const DEFAULT_LOGING_PATH = path.dirname(require.main.filename);

const winston = require('winston')

class Logger {

    constructor () {
        this.logUtil = winston.createLogger({
            level: 'info',
            format: winston.format.json(),
            transports: [
              //
              // - Write to all logs with level `info` and below to `combined.log` 
              // - Write all logs error (and below) to `error.log`.
              //
              new winston.transports.File({ filename: 'error.log', level: 'error' }),
              new winston.transports.File({ filename: 'system.log' })
            ]
          })

          if (process.env.NODE_ENV !== 'production') {
            this.logUtil.add(new winston.transports.Console({
              format: winston.format.simple()
            }));
            winston.addColors({
                info : 'green',
                error :  'red'
            })
          }

    }
     log (message, type='info') {
        this.logUtil.log(type, message)
    }

}

module.exports = Logger