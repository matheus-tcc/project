'use strict'

class Abstract {
    constructor() {
        
    }
    async connect() {}
    async createCollection() {}
    async collectionExists() {}
    async select() {}
    async insert() {}
    async update() {}
    async count() {}
}

module.exports = Abstract