'use strict'

const Abstract = Project.getClass('core/resource_database_adapter_abstract')

class Mongo extends Abstract {
    construct () {
        this.client = require('mongodb').MongoClient
    }

    async connect(databaseConfig) {
        if (!this.hasOwnProperty('client')) {
            this.client = require('mongodb').MongoClient
        }
        this.database = await this.client.connect(databaseConfig.host+'/'+databaseConfig.dbName)

        Project.dispatchEvent('mongo_database_connected')
        return this;
    }

    async createCollection(collectionName, extraData = {}) {
        let result = await this.database.createCollection(collectionName, extraData)

        Project.dispatchEvent('collection_created', result)

        return this
    }

    async collectionExists(collectionName) {
        let ret = this.database.listCollections({name: collectionName})
        return ret.hasNext()
    }

    async select(collectionName, condition, page = false, limit = false) {
        let result = await this.database.collection(collectionName).find(condition)
        
        if (page && limit) {
            result.skip((limit * page) - limit)
        }

        if(limit) {
            result.limit(limit)
        }
        
        let res = await result.toArray()
        return res
    }
    async insert(table, data) {
        let collection = await this.database.collection(table)
        return await collection.save(data)
    }

    async count(table, query, config) {
        let collection = await this.database.collection(table)
        return await collection.count(query, config)
    }

    async update(table, data) {
        let collection = await this.database.collection(table)
        return await collection.update({_id: data._id}, data,{upsert:true});
    }
}

module.exports = Mongo