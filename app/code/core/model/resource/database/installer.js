const Task = require('../../task')

const DEFAULT_SCOPE = 'global'

class Installer extends Task {
    createTables()  {
        Project.getApp().dispatcher.on('database_connected', () => {
            Project.log('Initiating module installation..')
            for (let tables of this.setup_data) {
                for(let table of tables.tables) {
                    Project.getApp().database.createTable(table.name, table.config)
                }
            }
        })
    }

    get setup_data () {
        let config = Project.getApp().module_data[DEFAULT_SCOPE]
        if (config.hasOwnProperty('setup')) {
            return config.setup
        }
        return []
    }
}

module.exports = Installer