'use strict'
const Task = require('../task')

class Database extends Task {

    async initiateDatabase() {
        var self = this
        Project.log('Initiating database connection')

        this.initiateAdapter(this.database_config.client)

        if (this.adapter) {
            this.adapter.connect(this.database_config).then((res)=>{
                Project.dispatchEvent('database_connected', self)
            });
        }
    }

    get database_config () {
        if (!Project.app.config.config.hasOwnProperty('database')) {
            throw new Error('No database configuration could be found')
        }
        return Project.app.config.config.database
    }

    initiateAdapter(client) {
        let model = Project.getModel('core/resource_database_adapter_'+client)
        this.adapter = model
        return this
    }

    async createTable(table, config) {
        if (await this.adapter.collectionExists(table)) {
            Project.log('Table: '+table+' already exists')
            return this
        }
        return await this.adapter.createCollection(table, config)
    }

    async select(table, id, key) {
        let condition = {} 
        condition[key] = id
        return await this.free_select(table, condition)
    }

    async free_select(table, condition) {
        return await this.adapter.select(table, condition)
    }

    async selectMultiple(table, condition={}, page = false, limit = false) {
        return await this.adapter.select(table, condition, page, limit)
    }

    async insertOrUpdate(table, data) {
        if (data.hasOwnProperty('_id')) {
            return await this.adapter.update(table, data)
        }
        return await this.adapter.insert(table, data)
    }

    async count(table, query, config) {
        return await this.adapter.count(table, query, config)
    }

}

module.exports = Database