'use strict'

const Abstract = Project.getClass('core/abstract')


class Collection extends Abstract {
    constructor (props) {
        super()
        let entity
        if (Array.isArray(props)) {
            entity = props[0]
        }
        this._entity = props[1]
        this._table = entity._table
        this._index = entity._index
        this._query = {}
        this._page = 1
        this._data = []
    }

    addFilter(filter) {
        this._query = Object.assign(filter, this._query)
        return this
    }

    clearFilter() {
        this._query = {}
    }

    set page(page) {
        page = parseInt(page)
        if (!Number.isInteger(page)) {
            throw new Error('Invalid page: '+ page)
        }
        this._page = page
    }

    set limit (limit) {
        limit = parseInt(limit)
        if (!Number.isInteger(limit)) {
            throw new Error('Invalid limit: '+ limit)
        }
        this._limit = limit
    }

    async load(elementId, key = null) {
        let data = await Project.getApp().database.selectMultiple(this._table, this._query, this._page, this._limit)
        this._data = data
        for (let key in this._data) {
            let newEntity = new this._entity()
            newEntity._data = this._data[key]
            newEntity._loaded = true
            this._data[key] = newEntity
        }
        return this
    }

    async count(query=null, config=null) {
        let data = await Project.getApp().database.count(this._table, query, config)

        return data

    }

    * iterate() {
        for (let i of this._data) {
            yield i
        }
    }
}


module.exports = Collection