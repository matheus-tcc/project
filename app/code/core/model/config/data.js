const Abstract = Project.getClass('core/abstract')

class Data extends Abstract {
    constructor () {
        super();
        this._table = 'core_config_data'
        this._index = 'path' 
        this._collection = Project.getModel('core/resource_collection', this, Data)
    }
}
module.exports = Data