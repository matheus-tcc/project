'use strict'
const _ = require('lodash')
class Abstract  {
    
    constructor () {
        this._table = ''
        this._index = ''
        this._data = {}
        this._origData = {}
        
    }

    async save () {
        Project.dispatchEvent(this._table+'_before_save', this)
        let insertResult = await Project.getApp().database.insertOrUpdate(this._table, this._data)
        if (insertResult.result.ok != 1) {
            throw new Error('Could not save entity')
        }
        Project.dispatchEvent(this._table+'_after_save', this)

        return this
    }

    async load(elementId, key = null) {
        if (key == null) {
            key = this._index
        }
        let data = await Project.getApp().database.select(this._table, elementId, key)
        this._loaded = true
        this._data = data.pop()
        return this
    }

    async loadByCondition(condition) {
        let data = await Project.getApp().database.free_select(this._table, condition)
        this._data = data.pop()
        this._origData = data.pop()
        this._loaded = true
        return this
    }

    toJson() {
        return JSON.stringify(this._data)
    }

    async collection (filter = {}, limit = false, page = false) {
        this._collection.addFilter(filter)
        if (page)
            this._collection.page = page
        if (limit)
            this._collection.limit = limit

        return await this._collection.load()
    }

    get table() {
        return this._table
    }

    
    get index() {
        return this._index
    }

    isLoaded() {
        return this._loaded && !_.isEmpty(this._data)
    }
    
}

module.exports = Abstract