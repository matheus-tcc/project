const moment = require('moment')

const Abstract = Project.getClass('core/abstract')

const SECURE_PROTOCOL = 'https'

const UNSECURE_PROTOCOL = 'http'

class Application extends Abstract {
    constructor () {
        super();
        this._table = 'api_application'
        this._index = 'domain' 
        this._collection = Project.getModel('core/resource_collection', this, Application)
    }

    get apiUrl () {
        return this._data.api_url
    }

    get domain () {
        return this._data.domain
    }

    get type () {
        return this._data.type
    }

    get is_secure () {
        return this._data.is_secure
    }

    get domain () {
        return this._data.domain
    }

    get manager_url() {
        let prefix = this.is_secure ? SECURE_PROTOCOL : UNSECURE_PROTOCOL
        prefix = prefix + "://"
        return prefix+this.domain
    }

    get delayed_action () {
        return this._data.hasOwnProperty('delayed_action') ? this._data['delayed_action'] : []
    }

    get delay_from () {
        if (this._data.hasOwnProperty('delay_from')) {
            if (typeof this._data.delay_from != 'object') {
                this._data.delay_from = new Date(this._data.delay_from)
            }
            return this.prepare_hour(this._data.delay_from)
        }

        return null
    }

    get delay_to () {
        if (this._data.hasOwnProperty('delay_to')) {
            if (typeof this._data.delay_to != 'object') {
                this._data.delay_to = new Date(this._data.delay_to)
            }
            return this.prepare_hour(this._data.delay_to)
        }
        
        return null
    }

    prepare_hour(date){
        return moment().hour(date.getHours()).minute(date.getMinutes())
    }

    shouldDelay() {
        if ( !this.delay_from || !this.delay_to ) {
            return false
        }
        if ( (this.delay_from.hour() < moment().hour() && this.delay_to.hour() > moment().hour() ) || 
        (this.delay_from.minute() < moment().minute() && this.delay_to.minute() > moment().minute() )) {
            return true
        }

        return false
    }

    hasReachedLimit() {
        let day = new Date()
        let currentDate = new Date(Date.UTC(day.getFullYear(),day.getMonth(), day.getDate()))
        if (!this._data.requests) {
            return false
        }
        for (let key in this._data.requests) {
            if (this._data.requests[key].date.getTime() == currentDate.getTime() && this._data.requests[key].count >= this._data.daily_limit) {
                return true
            }
        }

        return false
    }

    incRequestToday() {
        let day = new Date()
        let currentDate = new Date(Date.UTC(day.getFullYear(),day.getMonth(), day.getDate()))
        let foundDate = false
        if( !Array.isArray(this._data.requests) ) {
            this._data.requests = []
        }
        for (let key in this._data.requests) {
            if (this._data.requests[key].date.getTime() == currentDate.getTime()) {
                this._data.requests[key].count = parseInt(this._data.requests[key].count) + 1
                foundDate = true
            }
        }

        if (!foundDate) {
            this._data.requests.push({
                date : currentDate,
                count : 1
            })
        }
        return this
    }
}

module.exports = Application