const Abstract = Project.getClass('api/resource_abstract')

class Daily extends Abstract {
    async fetch(request) {
        let last = request.getParam('limit')
        let collection = Project.getModel('api_manager/consumer')
        let result = []
        collection = await collection.collection({action_history : {$exists : 1}})
        for (let consumer of collection._data) {
            let requests = consumer.daily_requests.slice(parseInt(last) * -1) 
            for (let req of requests) {
                let actualDate = new Date(Date.UTC(req.date.getFullYear(),req.date.getMonth(), req.date.getDate()))
                let id = false
                for (let key in result) {
                    if (result[key].date.getTime() == actualDate.getTime()) {
                        id = key
                    } 
                }
                if (!id) {
                    result.push({
                        date : actualDate,
                        requests : [
                            {
                                consumer : consumer._data.username,
                                quantity : req.requests.length
                            }
                        ]
                    })
                }
                else {
                    result[id].requests.push({
                        consumer : consumer._data.username,
                        quantity : req.requests.length
                    })
                }
            }
        }

        return result
    }
}

module.exports = Daily