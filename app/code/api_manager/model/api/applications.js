const Abstract = Project.getClass('api/resource_abstract')

const ENTITY = 'api_manager/application'

const LIMIT_PER_PAGE = 10

class Applications extends Abstract {

    async fetch(request) {
        let instance = Project.getModel(ENTITY)
        if (!request.params.id) {
            let currentPage = request.params.page ? request.params.page : 1
            let limit = request.params.limit ? request.params.limit : LIMIT_PER_PAGE
            let index = instance._index
            instance = await instance.collection({}, limit, currentPage)
            return {
                total_qty : await instance.count(),
                limit_page: LIMIT_PER_PAGE,
                current_page: currentPage,
                index:index,
                data : this.filter_attributes(instance._data)
            }
        }
        else {
            await instance.load(request.params.id)

            if (!instance.isLoaded()) {
                throw new Error('could not load entity')
            }

            return {
                index: instance._index,
                data : instance._data
            }



        }
    }

    async update(request) {
        let data = request.post

        if (!request.params.id) {
            throw new Error('invalid application')
        }
        let entity = Project.getModel(ENTITY)
        
        await entity.load(request.params.id)
        
        if (!entity.isLoaded()) {
            throw new Error('invalid application')
        }

        await this._save(entity, data)

        return entity
    }

    async create(request) {
        let data = request.post

        let entity = Project.getModel(ENTITY)

        await this._save(entity, data)

        return entity
    }

    filter_attributes (data) {
        let _forbiddenAttrs = [
            '_id'
        ]
        if (Array.isArray(data)) {
            for (let ins in data) {
                data[ins] = this.filter_attributes(data[ins]._data)
            }
        }
        else {
            for (let attr in data) {
                if (_forbiddenAttrs.includes(attr)) {
                    delete data[attr]
                }
            }
        }
        return data
    }

    async _save(entity, data) {

        entity._data = Object.assign(entity._data, data)

        return await entity.save()

    }

}

module.exports = Applications