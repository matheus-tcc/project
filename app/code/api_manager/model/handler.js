const perfy = require('perfy')
const sleep = require('sleep')
const uniqid = require('uniqid')

const PREVENT_EXECUTION = 'no_exec'

const DELAY_EXECUTION = 'delay_exec'

const QUEUE_EXECUTION = 'queue_exec'

const DEFAULT_DELAY_PERIOD = 3

const LIMIT_OF_CONCURENT_ACTIONS = 10

const LIMIT_OF_CONCURENT_ACTIONS_TO_PREVENT = 30

const DELAY_LIMIT = 3

const SHOULD_PASSTRHU = false

const HEADER_PREFIX = 'X-Api-Manager-'

class Handler {
    constructor() {
        this.flags = []
        this._unqId = uniqid()
        this._done = false
        this.ignoreActions = [] //can be altered by entensions
    }

    execute(req, res, application) {
        this.request = req
        
        this.response = res

        this._id = ''

        this.application = application
        /**
         * favicon treatment
         */
        if (this.isFavicon()) {
            this.response.status = 404
            this.response.endStream('')
            return this;
        }
        
        
        this._analyze().then((r)=>{
            Project.getSingleton('api_manager/manager').addRequest(this)
            r._preDispatch().then((x) => {
                x.wasDelayed = false
                if (this.flags.includes(PREVENT_EXECUTION)) {
                    let message = "Invalid credentials"
                    if (this.consumer.isLoaded()) {
                        if (this._limit_reached) {
                            message = 'Daily limit has been reached'
                        }
                        else {
                            message = 'Access not allowed'
                        }
                    }
                    this._denyExec(message)
                    return x._postDispatch()
                }

                Project.log('Current concurrent request limit: '+this.concurrent_limit_prevent)
                if (this._verifyPendingActions() > this.concurrent_limit_prevent ) {
                    this._denyExec('Too many requests')
                    return x._postDispatch()
                }
                
                let delayCounter = 0
                while (this.flags.includes(DELAY_EXECUTION) &&  //if this action is supposed to be delayed
                    this.application.shouldDelay() && //if the application has delay time configured
                    this._verifyPendingActions() > this.delay_with_qty 
                ) { // and if current actions breaks the limit of concurent action
                    if (delayCounter == this.delay_limit) {
                        if (!this.allow_passtrough) {
                            this.flags.push(PREVENT_EXECUTION)
                        }
                        break
                    }
                    this._delay()
                    x.wasDelayed = true
                    delayCounter++
                    
                }
    
                if (x.wasDelayed) {
                    this.response.header = {
                        'key' : 'X-Api-Manager-Delayed',
                        'value' : true
                    }
                }
                if (!this.flags.includes(PREVENT_EXECUTION)) {
                    x._execute().then((s) => {
                        s._postDispatch() 
                    })
                }    
            })
        })
    }
    /**
     * Denies the execution of the request, can be overwritten
     */
    _denyExec(message) {
        this.response.status = this.consumer.isLoaded() ? 429 : 400

        this.response.header = {
            'key' : 'X-Api-Manager-Prevented',
            'value' : true
        }
        
        return this
    }
    isFavicon() {
        return this.request.getPath().indexOf("favicon.ico") !== -1
    }
    /**
     * Executed before every handled request
     * @returns Handler
     */
    async _preDispatch() {
        perfy.start(this.id_api+"_"+this._unqId)

        let limit = await Project.getConfig('api_manager/management/concurrent_requests_limit')

        this.concurrent_limit_prevent = limit ? limit : LIMIT_OF_CONCURENT_ACTIONS_TO_PREVENT
        
        this.consumer = await this._loadConsumer()
        
        let shouldSend = await Project.getConfig('api_manager/personalization/send_headers')

        this.delay_sec = await Project.getConfig('api_manager/personalization/default_delay')

        this.delay_with_qty = await Project.getConfig('api_manager/management/start_delaying_with_qty')
        
        this.delay_with_qty = this.delay_with_qty ? this.delay_with_qty : LIMIT_OF_CONCURENT_ACTIONS

        this.delay_limit = await Project.getConfig('api_manager/management/delay_limit')

        this.delay_limit = this.delay_limit ? parseInt(this.delay_limit) : DELAY_LIMIT

        this.allow_passtrough = await Project.getConfig('api_manager/management/allow_pass')

        this.allow_passtrough = this.allow_passtrough  ?  this.allow_passtrough : SHOULD_PASSTRHU
        

        if (shouldSend != 0) {
            this.response.header = {
                'key' : 'X-Api-Manager-Managing',
                'value' : true
            }
        }
        if ( this.consumer.isLoaded() && shouldSend != 0) {
            this.response.header = {
                'key' : 'X-Api-Manager-Request-Limit',
                'value' : this.consumer.daily_limit
            }
    
            this.response.header = {
                'key' : 'X-Api-Manager-Requests-Executed',
                'value' : this.consumer.requests_today.length
            }
        }

        if (this.application.delayed_action.includes(this.id_api)) {
            this.add_flag(DELAY_EXECUTION)
        }

        if (this.ignoreActions.includes(this.id_api)) {
            return this
        }
        if (!this.consumer.isLoaded() || //if no consumer was found
            !this.consumer.allowed_actions.includes('all') &&  //if consumer cannot access all actions
            !this.consumer.allowed_actions.includes(this.id_api)){ //if consumer cant access this action
                this.add_flag(PREVENT_EXECUTION) //prevent execution
        } 
        else if (this.consumer.hasReachedLimit()) { //if consumer has reached limit of daily requests
            this._limit_reached = this.consumer.hasReachedLimit()
            this.add_flag(PREVENT_EXECUTION) //prevent execution
        }
        return this
    }

    _verifyPendingActions() {
        let requests = Project.getSingleton('api_manager/manager')._requests[this.application.domain]
        if (requests) {
            return requests.length
        }

        return 0
        
    }
    /**
     * executed after every request execution
     */
    _postDispatch() {
        let result = perfy.end(this.id_api+"_"+this._unqId)
        
        this._execution_time = result.time
        
        Project.log('Api action: '+ this.id_api+' time: '+ result.time + ' denied : ' + this.flags.includes(PREVENT_EXECUTION))

        Project.getSingleton('api_manager/manager').removeRequest(this)
    }

    _delay() {
        Project.log('Delaying action: #'+this.id+' for'+this.delay_sec ? this.delay_sec : DEFAULT_DELAY_PERIOD+' execution to prevent application degradation')
        sleep.sleep(this.delay_sec ? parseInt(this.delay_sec) : DEFAULT_DELAY_PERIOD)

    }

    async _execute() {

    }

    get id_api() {
        return this._id
    }

    async _analyze(){
        return this
    }

    add_flag (flag) {
        this.flags.push(flag)
    }

    get id () {
        if (!this._id_process) {
            this._id_process = uniqid()
        }
        
        return this._id_process
    }

    async _loadConsumer() {
        let appKey, appId = null
        for (let key in this.request.headers ) {
            if (key.toLowerCase() == (HEADER_PREFIX+'Key').toLowerCase()) {
                appKey = this.request.headers[key]
            }
            else if (key.toLowerCase() == (HEADER_PREFIX+'Id').toLowerCase()) {
                appId = this.request.headers[key]
            }
        }
        Project.dispatchEvent('api_manager_consumer_loaded_before', this)
        /**
         * @var consumer Consumer
         */
        let consumer = Project.getModel('api_manager/consumer')
        
        await consumer.loadByKeyAndId(appKey, appId)

        Project.dispatchEvent('api_manager_consumer_loaded_after', consumer)

        return consumer

    }

    get request_body() {
        return ''
    }
}

module.exports = Handler