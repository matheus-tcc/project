
const Server = Project.getClass('web/server')

const url = require('url');

class Api extends Server {
    initiateRoutes () {

    }

    handleRequest(req, res) {
        let host = req.headers.host
        var queryObject = url.parse(req.url,true).query;
        req.query = JSON.parse(JSON.stringify(queryObject))
        if (host.indexOf(":") != -1) {
            host = host.substring(0, host.indexOf(":"))
        }
        req = this._wrapRequest(req)
        res = this._wrapResponse(res)
        let application = Project.getModel('api_manager/application')
        if (!host) {
            throw new Error('invalid host')
        }
        application.load(host).then((v)=>{
            if (!v.isLoaded()) {
                throw new Error('Invalid Application')
            }
            let handler = Project.getSingleton('api_manager/manager').loadHandlerByApplication(v)
            if (!handler) {
                throw new Error('Invalid handler for this API')
            }
            handler.execute(req, res, v)
        }).catch((err) => {
            res.endStream({message:err.message})
        })
    }
}

module.exports = Api
