
const DEFAULT_CLEANING_SCHEDULE = 172800000 // = 48h

const OLD_DATA_LIMIT_IN_DAYS = 10

const DEFAULT_SCOPE = 'global'

var fs = require('fs');

const zlib = require('zlib');

class Cleaner {
    
    async init() {
        let timeout = DEFAULT_CLEANING_SCHEDULE
        let storeData = true && await Project.getConfig('api_manager/cleaner/save_old_records') == "1"
        setInterval(() => {
            Project.log('API Manager Cleaner - Cleaning old data to improve performance')
            this.clearOldData().then((oldData) => {
                if (storeData && oldData) {
                    this.storeData(oldData).then(() =>{
                        Project.log('API Manager Cleaner - OK')
                    })
                }
            }).catch(err => Project.log(err.message, 'error'))
        }, timeout) 
    }

    async clearOldData() {
        let data = Project.getApp().module_data[DEFAULT_SCOPE].cleaner
        let configDataLimit = await Project.getConfig('api_manager/cleaner/remove_older_than')
        let oldDataLimt = configDataLimit ? configDataLimit : OLD_DATA_LIMIT_IN_DAYS
        let limitDate = new Date()
        limitDate.setDate(limitDate.getDate() - parseInt(oldDataLimt))

        if (!data) {
            throw new Error('No data to clean')
        }

        let dataToStore = {}

        for (let ent of data) {
            let collection = await Project.getModel(ent.entity).collection()
            for (let entity of collection._data) {
                let keysToRemove = []
                for (let key in entity._data[ent.attribute]) {
                    if (entity._data[ent.attribute][key][ent.date_attribute] < limitDate) {
                        if (!dataToStore[ent.entity]) {
                            dataToStore[ent.entity] = {
                                id : entity._data[entity._index],
                                data : []
                            }
                        }
                        dataToStore[ent.entity].data.push(entity._data[ent.attribute][key])
                        keysToRemove.push(key)
                    }
                }

                for(let k in keysToRemove) {
                    delete entity._data[ent.attribute][k]
                }
                entity._data[ent.attribute] = entity._data[ent.attribute].filter((v) => {return v})
                await entity.save()
            }
        }
        return dataToStore

    }

    async storeData(data) {
        let path = "./var/"

        if (!this.directoryExists(path)) {
            fs.mkdirSync(path)
        }
        let shouldCompress = await Project.getConfig('api_manager/cleaner/compress_saved_old_data')

        let fileName = 'cleaner_'+new Date().getTime()+'.json'
        
        fs.writeFile(path+fileName, JSON.stringify(data), function(err) {
            if(err) {
                return Project.log(err.message, 'error');
            }
        
            Project.log("API Manager - Cleaner File saved successfully")

            if ( shouldCompress == "1") {
                if (!fs.existsSync(path+fileName)) {
                    return Project.log('API Manager - Cleaner Nothing to compress')
                }
                const gzip = zlib.createGzip();
                
                var inp = fs.createReadStream(path+fileName);
                
                var out = fs.createWriteStream(path+fileName+'.gz');
        
                inp.pipe(gzip).pipe(out);
    
                fs.unlink(path+fileName, () => {
                    Project.log("API Manager - Cleaner File compressed successfully")
                })
    
            }
        }); 

        

    }

    directoryExists(path) {
        try {
            return fs.lstatSync(path).isDirectory()
        }
        catch (err) {
            return false
        }
    }
}

module.exports = Cleaner