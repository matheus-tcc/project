const Task = Project.getClass('core/task')

class Manager extends Task {
    constructor() {
        super()
        this._requests = []
        this._cleaner = Project.getModel('api_manager/manager_cleaner')
    }
    async initiateApi() {
        Project.getApp().dispatcher.on('database_connected', () => {
            Project.log('Initiating API Managing...')
            let apiManager = Project.getModel('api_manager/manager_api', 3001)
            try {
                apiManager.startServer()
                Project.log("Api Manager - OK")
                this._cleaner.init()
                Project.log("Api Manager Cleaner - OK")
            }
            catch (err) {
                console.log(err)
            }
        });

    }

    loadHandlerByApplication (application) {
        let searchableScopes = ['global'] 
        for (let scope of searchableScopes) {
            if (Project.app.module_data[scope].hasOwnProperty('api_manager')) {
                let apiManager = Project.app.module_data[scope]['api_manager']
                for (let handle of apiManager) {
                    if (application.type == handle.type) {
                        return Project.getModel(handle.moduleName+"/"+handle.handler)
                    }
                }
            }
        }
        return null
    }

    addRequest(request) {
        if (!this._requests.hasOwnProperty(request.application.domain)) {
            this._requests[request.application.domain] = []
        }
        this._requests[request.application.domain].push(request)
        return this
    } 

    removeRequest (request) {
        let key
        for (key in this._requests[request.application.domain]) {
            let req = this._requests[request.application.domain][key]
            if (req.id == request.id) {
                if (this._requests[request.application.domain][key].consumer.isLoaded()) {
                    /**
                     * consumer handling
                     */
                    this._requests[request.application.domain][key].consumer.addRequest(req)
                    this._requests[request.application.domain][key].consumer.save()
                    /**
                     * application handling 
                     */
                    this._requests[request.application.domain][key].application.incRequestToday()
                    this._requests[request.application.domain][key].application.save()
                }
                break
            }
        }
        delete this._requests[request.application.domain][key]
        this._requests[request.application.domain] = this._requests[request.application.domain].filter((e) => {return e} ) // remove empty values from array
        return this
    }
}

module.exports = Manager