class Types {
    async toOptionArray() {
        let types = Project.app.module_data['global']['api_manager']
        let ret = []

        for (let type of types) {
            ret.push({
                'label' : type.type.charAt(0).toUpperCase() + type.type.substr(1),
                'value' : type.type
            })
        }
        return ret
    }
}

module.exports = Types