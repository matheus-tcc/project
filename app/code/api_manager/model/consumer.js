const Abstract = Project.getClass('core/abstract')

class Consumer extends Abstract {
    constructor () {
        super();
        this._table = 'api_consumer'
        this._index = 'username'
        this._collection = Project.getModel('core/resource_collection', this, Consumer)
    }

    async loadByKeyAndId(key, id) {
        let condition = {
            'application_key' : key,
            'application_id' : id
        }

        return await this.loadByCondition(condition)
    }

    get daily_requests () {
        if (!this._data.action_history){
                this._data.action_history = []
        }
        return this._data.action_history
    }

    get allowed_actions () {
        return this._data.allowed_actions
    }

    get daily_limit() {
        return this._data.daily_limit
    }

    hasReachedLimit() {
        if (this.requests_today.length >= this.daily_limit) {
            return true
        }
        return false;
    }

    get requests_today() {
        let day = new Date()
        let today = new Date(Date.UTC(day.getFullYear(),day.getMonth(), day.getDate()))
        if (!this.daily_requests)
                return []
        for ( let history of this.daily_requests ){
            let date = history.date
            if (!(date instanceof Date)) {
                date = this._convertDate(history.date.high_)
            }
            if (date.getTime() == today.getTime()) {
                return history.requests
            }
        }
        return []
    }

    _convertDate(date) {
        var epoch=date

        if((date >= 100000000000000)||(date <= -100000000000000)){
            epoch=Math.round(date/1000000)
            date=Math.round(date/1000)
        }else if((date >= 100000000000)||(date <= -100000000000)){

            epoch=Math.floor(date/1000)
            rest = date - (epoch*1000)
        }else{

            if(date > 10000000000)extraInfo=1
            date=(date*1000)
        }
        return new Date(date)
    }

    addRequest(request) {
        let objToAdd = {
            'body' : request.request_body,
            'datetime' : new Date(),
            'delayed' : request.wasDelayed,
            'execution_time' : request._execution_time,
            'action_id' : request.id_api,
            'application': request.application.domain
        }
        let day = new Date()
        let today = new Date(Date.UTC(day.getFullYear(),day.getMonth(), day.getDate()))
        for ( let key in this.daily_requests ){
            let date = this.daily_requests[key].date
            if (!(date instanceof Date)) {
                date = this._convertDate(this.daily_requests[key].date.high_)
            }
            if (date.getTime() == today.getTime()) {
                this.daily_requests[key].requests.push(objToAdd)
                return this
            }
        }
        this.daily_requests.push({
            'date' : today,
            'requests' : [
                objToAdd
            ]
        })
    }
}

module.exports = Consumer