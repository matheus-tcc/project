'use strict'

const Action = Project.getClass('web/router_action')

class Consumers extends Action {
    async indexAction() {
        this.response.endStream(
            await Project.getSingleton('api/api').execute('api_consumers', this.request, this.response)
        )
    
    }
}

module.exports = Consumers