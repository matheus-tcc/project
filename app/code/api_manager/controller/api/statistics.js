const Action = Project.getClass('web/router_action')

class Statistics extends Action {
    async dailyAction() {
        this.response.endStream(
            await Project.getSingleton('api/api').execute('api_statistics_daily', this.request, this.response)
        )
    }
}

module.exports = Statistics