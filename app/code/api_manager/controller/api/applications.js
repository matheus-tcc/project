'use strict'

const Action = Project.getClass('web/router_action')

class Application extends Action {
    async indexAction() {
    
        this.response.endStream(
            await Project.getSingleton('api/api').execute('api_applications', this.request, this.response)
        )
    
    }
}

module.exports = Application