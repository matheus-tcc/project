const rp = require('request-promise')

class Http {
    execute(application, request) {
        let headers = request.headers
        let url = new URL(application.apiUrl);
        headers.host = url.host
        delete headers['accept-encoding']
        var options = {
            method: request.method,
            uri: application.apiUrl+request.path,
            body: request.rawPost,
            headers: headers,
            insecure: true,
            rejectUnauthorized: false,
            resolveWithFullResponse : true
        }
        return rp(options)
    }
}

module.exports = Http