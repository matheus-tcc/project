
const Handler = Project.getClass('api_manager/handler')

const o2x = require('object-to-xml');

class Request extends Handler {
    
    async _preDispatch() {
        this.ignoreActions = ["no_route"]
        await super._preDispatch()
        return this
    }
    async _execute() {
        await this.executeRequest()

        return this
    }

    async _analyze() {
        let path = this.request.path
        
        if (path == "/") {
            this._id = "no_route"
            return this
        }

        let resource = path.split('/').filter(word => word.length  > 0)

        this._id = resource[0]
        
        return this
    }

    _denyExec(message) {
        super._denyExec(message)

        let response = {
            'error' : {
                'message' : message
            }
        }

        let encoding = null

        if (this.request.headers['content-type']) {
            encoding = this.request.headers['content-type'].split('/')[1]
        }
    
        switch (encoding) {
            case 'xml' : {
                this.response.header = {
                    'key' : 'content-type',
                    'value' : 'application/xml'
                }
                this.response.endStream(o2x(response))
                break;
            }
            default:
            case 'json': {
                this.response.header = {
                    'key' : 'content-type',
                    'value' : 'application/json'
                }
                this.response.endStream(response)
                break
            }
        }   

        return this
    }

    get request_body() {
        let result = {
            'method' : this.request.method,
            'path' : this.request.path,
            'params' :this.request.params,
            'payload' : this.request.rawPost,
            'headers' : this.request.headers
        }

        return result
    }

    async executeRequest() {
        let client = Project.getModel('rest_api/request_http');
        try {
            let response  = await client.execute(this.application, this.request)
            if (response.headers['content-type']) {
                this.response.header = {
                    'key' : 'content-type',
                    'value' : response.headers['content-type']
                }
            }
            this.response.endStream(response.body)
        }
        catch (err) {
            if (!err.response) {
                this.response.status = 500
                this.response.endStream(err.error)
            }
            else {
                if (err.response.headers['content-type']) {
                    this.response.header = {
                        'key' : 'content-type',
                        'value' : err.response.headers['content-type']
                    }
                }
                this.response.status = err.statusCode
                this.response.endStream(err.error)
            }
        }

        return this
    }


}

module.exports = Request