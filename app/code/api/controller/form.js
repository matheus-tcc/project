'use strict'

const Action = Project.getClass('web/router_action')

class Grid extends Action {
    async indexAction() {
        this.response.endStream(
            await Project.getSingleton('api/api').execute('form', this.request, this.response)
        )
    }
}

module.exports = Grid