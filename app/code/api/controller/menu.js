'use strict'

const Action = Project.getClass('web/router_action')

class Menu extends Action {
    async indexAction() {
        this.response.endStream(
            await Project.getSingleton('api/api').execute('menu', this.request, this.response)
        )
    }
}

module.exports = Menu