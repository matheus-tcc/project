'use strict'

const Action = Project.getClass('web/router_action')

class Login extends Action {
    async indexAction() {
        this.response.endStream(
            await Project.getSingleton('api/api').execute('login', this.request, this.response, false)
        )
    }
}

module.exports = Login