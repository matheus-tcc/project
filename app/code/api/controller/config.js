'use strict'

const Action = Project.getClass('web/router_action')

class Config extends Action {
    async indexAction() {
        this.response.endStream(
            await Project.getSingleton('api/api').execute('resource_config', this.request, this.response)
        )
    
    }
    async valueAction() {
        this.response.endStream(
            await Project.getSingleton('api/api').execute('resource_config_value', this.request, this.response)
        )
    
    }
}

module.exports = Config