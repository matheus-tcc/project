'use strict'

const Task = Project.getClass('core/task')

class Main extends Task {
    /**
     * @description Initiates api server
     */
    async defaultTask() {
        let server = Project.getModel('api/handler')
        server.start()

    }

    async secondaryTask() {
        
    }
}

module.exports = Main