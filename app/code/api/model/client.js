const Abstract = Project.getClass('core/abstract')

const bcrypt = require('bcrypt')

const uniqid = require('uniqid')

const SESSION_TIME_PERIOD_DAYS = 2

class Client extends Abstract {
    constructor () {
        super();
        this._table = 'api_client'
        this._index = 'username' 
        this._collection = Project.getModel('core/resource_collection', this)
    }

    async createSession(source) {
        let sessionId = ''
        if (this.active_session) {
            Project.log('Client - Killing previous session')
            await this.killSessions()
        }
        
        Project.log('Client - Creating session')
        sessionId = uniqid()
        let sessionData = {
            session_id : sessionId,
            source : source,
            creation_date : new Date(),
            is_active : true,
            history : [

            ]
        }

        this.addSession(sessionData)
        await this.save()
        


        return sessionId
        
    }

    validatePassword(password) {
        return bcrypt.compareSync(password, this._data.password)
    }

    get salt() {
        return Project.app.config.config.database.salt
    }

    _hashPassword() {
        let salt = bcrypt.genSaltSync(this.salt)
        let hashedPassword = bcrypt.hashSync(this._data.password, salt)
        return hashedPassword
    }

    async save() {
        if (!this.isLoaded()) {
            this._data.password = this._hashPassword()
        }

        return await super.save();
    }

    get active_session () {
        if (this._data.sessions){
            for (let session of this._data.sessions ) {
                if (session.is_active) {
                    return session
                }
            }
        }

        return false
    }

    addSession(session) {
        if (!this._data.sessions) {
            this._data.sessions = []
        }
        this._data.sessions.push(session)
    }

    async loadBySessionId (sessionId) {
        return await this.loadByCondition({sessions: {$elemMatch: {session_id: sessionId}}})
    }

    async validateSession(sessionId) {
        if (this.active_session.session_id == sessionId) {
            let sessionDate = this.active_session.creation_date
            sessionDate.setDate(sessionDate.getDate() + SESSION_TIME_PERIOD_DAYS)
            let currentDate = new Date()
            if (sessionDate < currentDate) { // if session date with period is new than today`s date we kill it 
                for (let key in this._data.sessions ) {
                    this._data.sessions[key].is_active = false
                }
                await this.save()

            }
            else {
                return this
            }
        }
        throw new Error('Invalid Session')
    }

    async killSessions() {
        console.log(this._data.sessions)
        for (let k in this._data.sessions) {
            this._data.sessions[k].is_active = false
        }
        return await this.save()
    }
}


module.exports = Client