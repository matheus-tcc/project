class Abstract {
    async execute(request, response) {
        switch(request.method) {
            case "GET":
                return await this.fetch(request)
            case "PUT":
                return await this.update(request)
            case "POST":
                return await this.create(request)
            case "DELETE":
                return await this.delete(request)
            case "OPTIONS":
                response.status = 204
                response.endStream('')
                break
                
        }
    }
}

module.exports = Abstract