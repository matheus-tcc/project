const Abstract = Project.getClass('api/resource_abstract')

const DEFAULT_CONFIGURATION_SCOPE = 'backend'

class Config extends Abstract {

    async fetch(request) {
        let config = Project.getApp().module_data[DEFAULT_CONFIGURATION_SCOPE]

        if (!config.settings) {
            throw new Error('could not load settings')
        }
        let result = []
        for (let setting of config.settings) {
            let response = []
            for (let section of setting.sections) {
                section.groups = setting.groups.map((group) => {
                    if (group.section == section.id) {
                        group.fields = setting.fields.map((field) => {
                            if (field.group == group.id) {
                                return field
                            }
                        }).filter(word => word != null)
                        
                        return group
                    }
                })
                response.push(section)
            }
            result.push(response.pop())
        }
        for (let k in result) {
            for (let y in result[k]['groups']) {
                for (let x in result[k]['groups'][y]['fields']) {
                    if (result[k]['groups'][y]['fields'][x].hasOwnProperty('options') && typeof result[k]['groups'][y]['fields'][x]['options'] == 'string' ) {
                        result[k]['groups'][y]['fields'][x]['options']  = await Project.getModel(result[k]['groups'][y]['fields'][x]['options']).toOptionArray()
                    }
                }
            }
        }
        return result;
    }

    async update(request) {

    }

    async create(request) {

    }

}

module.exports = Config