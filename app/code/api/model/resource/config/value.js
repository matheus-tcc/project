const Abstract = Project.getClass('api/resource_abstract')

class Value extends Abstract {
    async fetch(request) {
        let path = request.getParam('path')

        let config = Project.getModel('core/config_data')
        
        if (path) {
            await config.load(path)
            return config._data
        }
        else {
            let section = request.getParam('section')
            let filter = {}
            if (section) {
                filter = {'path' : new RegExp("^"+section+"\/")}
            }
            config = await config.collection(filter)
        }
        let data = []
        for (let cfg of config._data) {
            data.push(cfg._data)
        }
        return data
    }
    async update(request) {
        let requestData = request.post

        if (Array.isArray(requestData)) {

            for (let config of requestData) {
                await this._save(config)
            }
        }
        else {
            await this._save(requestData)
        }

    }

    async create(request) {
        let requestData = request.post

        if (Array.isArray(requestData)) {

            for (let config of requestData) {
                await this._save(config)
            }
        }
        else {
            await this._save(requestData)
        }
    }

    async _save(config) {
        let entity = Project.getModel('core/config_data')
        
        await entity.load(config.path)

        if (!entity.isLoaded()) {
            entity = Project.getModel('core/config_data')
        }

        for (let key in config) {
            entity._data[key] = config[key]
        }

        await entity.save()
    }
}

module.exports = Value