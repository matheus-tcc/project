class TrueOrFalse {
    async toOptionArray() {
        return [
            {
                'label' : 'Yes',
                'value' : 1
            },
            {
                'label' : 'No',
                'value' : 0
            }
        ]
    }
}

module.exports = TrueOrFalse