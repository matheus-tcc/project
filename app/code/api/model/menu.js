const Abstract = Project.getClass('api/resource_abstract')

const DEFAULT_SCOPE_FOR_MENUS = 'backend'

class Menu extends Abstract {

    async fetch(request) {
        let moduleData = Project.getApp().module_data[DEFAULT_SCOPE_FOR_MENUS]
        
        if (moduleData.menu) {
            return moduleData.menu.sort((a,b) => {
                if (a.position > b.position) {
                    return 1
                }
                else if (a.position = b.position) {
                    return 0
                }
                else {
                    return -1
                }
            })
        }

        return []
    }

    async update(request) {
        
    }

    async create(request) {

    }

}

module.exports = Menu