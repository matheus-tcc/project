'use strict'

const DEFAULT_SCOPE = 'global'

const DEFAULT_SESSION_HEADER = 'x-api-manager-session'

class Api {

    async initialize() {
        Project.log('Initializing API Module')
        this.api_config
        this._initialized = true
        Project.log('API Module - OK')
        Project.dispatchEvent('api_module_done', this)
    }

    get api_config () {
        if (!this._api_config) {
            let module_data = Project.app.module_data[DEFAULT_SCOPE]
            if (module_data.hasOwnProperty('api')) {
                this._api_config = module_data.api
            }
        }

        return this._api_config
    }
    async validateSession(sessionId) {
        let client = Project.getModel('api/client')
        
        await client.loadBySessionId(sessionId)

        await client.validateSession(sessionId)
    }

    async execute (entity, request, response, validateSession=true) {
        response.header = {
            "key" : "Content-Type",
            "value" : "application/json"
        }
        if (request.method.toLowerCase() !== "options" && validateSession) {
            try {
                if (!request.headers.hasOwnProperty(DEFAULT_SESSION_HEADER)) {
                    throw new Error('no session was defined')
                }
                await this.validateSession(request.headers[DEFAULT_SESSION_HEADER])
            }
            catch (err) {
                response.status = 403
                return {success:false, message :err.message}
            }
        }
        
        for (let api of this.api_config){

            if (api.entity == entity) {
                try {
                    if (!api.allow.includes(request.method.toLowerCase())) {
                        throw new Error('Method is not allowed')
                    }

                    let model = Project.getSingleton(api.moduleName+"/"+api.entity)

                    return await model.execute(request, response)
                }
                catch ( err ) {
                    response.status = 500
                    return {success:false, message :err.message}
                }
            }
        }

        throw new Error('Invalid API')
    }

}

module.exports = Api