const Abstract = Project.getClass('api/resource_abstract')

const DEFAULT_SCOPE_FOR_GRIDS = 'backend'

class Grid extends Abstract {

    async fetch(request) {
        let grid_id = request.getParam('grid_id')
        if (!grid_id) {
            throw new Error('no grid id was informed')
        }
        let data = Project.getApp().module_data[DEFAULT_SCOPE_FOR_GRIDS]
        if (data) {
            for (let page of data.pages) {
                if (page.hasOwnProperty('grid')) {
                    for (let grid of data.pages) {
                        let gridInstances = grid.grid
                        for (let gridInstance of gridInstances) {
                            console.log(gridInstance)
                            if (gridInstance.grid_id == grid_id) {
                                return gridInstance
                            }
                        }
                    }
                }
            }
        }

        return {}
        

    }

    async update(request) {

    }

    async create(request) {

    }

}

module.exports = Grid