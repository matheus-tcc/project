const Abstract = Project.getClass('api/resource_abstract')

const DEFAULT_SCOPE_FOR_FORMS = 'backend'

class Form extends Abstract {

    async fetch(request) {
        let form_id = request.getParam('form_id')
        if (!form_id) {
            throw new Error('no form id was informed')
        }
        let data = Project.getApp().module_data[DEFAULT_SCOPE_FOR_FORMS]
        if (data) {
            for (let page of data.pages) {
                if (page.hasOwnProperty('form')) {
                    for (let form of data.pages) {
                        let formInstances = form.form
                        for (let formInstance of formInstances) {
                            if (formInstance.form_id == form_id) {
                                for( let key in formInstance.fields )  {
                                    if (formInstance.fields[key].hasOwnProperty('options') && typeof formInstance.fields[key]['options'] == 'string' ) {
                                        formInstance.fields[key]['options']  = await  Project.getModel(formInstance.fields[key]['options']).toOptionArray()
                                    }
                                }
                                return formInstance
                            }
                        }
                    }
                }
            }
        }

        return {}
        

    }

    async update(request) {

    }

    async create(request) {

    }

}

module.exports = Form