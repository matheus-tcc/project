const Abstract = Project.getClass('api/resource_abstract')

class Login extends Abstract {

    async update(request) {
        let body = request.post

        if (!body) {
            throw new Error('No data was sent')
        }

        if ( !body.username && !body.password ) {
            throw new Error('Invalid username or password')
        }
        let model = await Project.getModel('api/client')

        model = await model.load(body.username)

        if (!model.isLoaded()) {
            throw new Error('Invalid username or password')
        }

        if (!model.validatePassword(body.password)) {
            throw new Error('Invalid username or password')
        }

        let session = {
            success : true,
            sessionId : await model.createSession(request.headers.host)
        }

        return session
    }

}

module.exports = Login